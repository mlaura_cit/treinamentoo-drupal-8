<?php require 'layout/header.php';?>
<?php 

require 'library/functions.php';

if($_SERVER['REQUEST_METHOD']== 'POST') {
    
    if (isset($_POST['name'])) {
        $name=$_POST['name'];
    } else {
        $name='';
    }
    if (isset($_POST['breed'])) {
        $breed=$_POST['breed'];
    } else {
        $breed='';
    }
    if (isset($_POST['weight'])) {
        $weight=$_POST['weight'];
    } else {
        $weight='';
    }
    if (isset($_POST['bio'])) {
        $bio=$_POST['bio'];
    } else {
        $bio='';
    }

    $pets=get_pets();
    $newPet=array (
        'name'=>$name,
        'breed'=>$breed,
        'weight'=>$weight,
        'bio'=>$bio,
        'age'=>'',
        'image'=>'Unknown',
    );
    $pets[]=$newPet;
   

    $json=json_encode($pets, JSON_PRETTY_PRINT);
    file_put_contents('data/pets.json', $json);

    header('Location: /');
    die;

}
?>
<div class= "container">
    <div class = "row">
        <div class= "col-xs-6">
            <h1> Add your pet!</h1>

            <form action="/add_pet.php" method= "post">
                       
                <div class="form-group">
                    <label for="pet-name"class="control-label">Pet Name</label>
                    <input type="text"name="name"id="pet-name"class="form-control" />
                </div>
                
                <div  class="form-group">
                    <label for="pet-breed"class="control-label">Breed </label>
                    <input type="text"name="breed"id="pet-breed"class="form-control" />
                </div>
               
                <div class="form-group">
                    <label for="pet-weight" class="control-label"> Weight (lbs)</label>
                     <input type="number" name="weight" id="pet-weight" class="form-control" />
                </div>
                
                <div class ="form-group">
                    <label for="pet-bio" class="control-label"> Pet Bio</label>
                     <textarea type="text" id="pet-bio" class="form-control" /></textarea>
                </div>

                <button type="submit" class="btn btn-primary">  
                    Add  
                    <span class="glyphicon glyphicon-heart"></span>

                </button>

            </form>
           
        </div>
    </div>
</div>

<?php require 'layout/footer.php';?>  